# MobileAppAutomationTest
 write pretty and stable Android UI tests with [**Espresso**](https://developer.android.com/training/testing/espresso/index.html) for the given Android source code / app.

✏️ **Task**:
* Login case (username: ***whiteelephant261***, password: ***video***).
* Search for "***sa***", select "***Sarah Friedrich***" from results, then click the call button.
* Deploy the tests on [**CircleCI**](https://circleci.com/).

